class FuzzySet:
	def __init__(self):
		self.data = {}

	def membership(self, key):
		if key in self.data:
			return self.data[key]
		else:
			return 0
			
	def set_data(self, data):
		for k, v in data:
			self.data[k] = v
	
	def new_point(self, k, v):
		self.data[k] = v

	def __str__(self):
		return str(self.data)
	
	def print_data(self):
		print(self.data)
		
	def union(self, other):
		nfs = FuzzySet()
		for k in set(list(self.data.keys()) + list(other.data.keys())):
			nfs.new_point(k, max(self.membership(k), other.membership(k)))
		
		return nfs
	
	def intersection(self, other):
		nfs = FuzzySet()
		for k in set(list(self.data.keys()) + list(other.data.keys())):
			nfs.new_point(k, min(self.membership(k), other.membership(k)))
		
		return nfs
		
	def complement(self):
		nfs = FuzzySet()
		for k in self.data.keys():
			nfs.new_point(k, round(1-self.membership(k), 3))
		
		return nfs
		
	def difference(self, other):
		return fs1.intersection(fs2.complement())

if __name__ == '__main__':
	data1 = [(int(line.split()[0]), float(line.split()[1])) for line in open('data1.txt').readlines()]
	data2 = [(int(line.split()[0]), float(line.split()[1])) for line in open('data2.txt').readlines()]

	fs1 = FuzzySet()
	fs1.set_data(data1)
	print('Fuzzy Set A is', fs1)
	
	fs2 = FuzzySet()
	fs2.set_data(data2)
	print('Fuzzy Set B is', fs2)
	
	while 1:
		choice = int(input('''
		
		MENU
		1. Union of A and B
		2. Intersection of A and B
		3. Complement of A
		4. Complement of B
		5. Difference of A and B
		6. Exit
		'''))
	
		if choice == 1:
			print(fs1.union(fs2))
		elif choice == 2:
			print(fs1.intersection(fs2))
		elif choice == 3:
			print(fs1.complement())
		elif choice == 4:
			print(fs2.complement())
		elif choice == 5:
			print(fs1.difference(fs2))
		elif choice == 6:
			print(' End ')
			break
		
