public class GeneticAlgorithmClassifier {
    public static void main(String[] args) {
        final int P = 50; // population size
        final int N = 32; // chromosome length
        final double C = 0.95; // crossover rate
        final double M = (double) 1 / P; // mutation rate
        final int G = 50; // # of generations
        GA ga = new GA(P, N, C, M);
        // Initialise population of random individuals
        Individual[] population = ga.initPop();
        // "Counting ones" fitness evaluation
        System.out.println("GEN0");
        ga.evaluatePop(population);
        ga.printFitness(population);

        int generation = 1;
        for (int i = 0; i < G; i++) {
            System.out.println("\nGeneration: " + generation);
            // Tournament selection
            population = ga.tournament(population);
            // Tournament winners fitness evaluation
            ga.evaluatePop(population);

            // Single-point crossover
            population = ga.crossover(population);
            // Crossover children fitness evaluation
            ga.evaluatePop(population);
            // Bit-wise mutation
            population = ga.mutate(population);
            // Post-mutation population fitness evaluation
            ga.evaluatePop(population);
            // Elitism replacement (remove the worst gene and replace with a copy of the best)
            population = ga.elitism(population);
            // Post-elitism population fitness evaluation
            ga.evaluatePop(population);
            ga.printFitness(population);
            generation++;
            if (ga.bestFitness(population) == N) {
                break;
            }
        }
    }

    public class GA {
        int populationSize;
        int chromosomeSize;
        double crossoverRate;
        double mutationRate;
        Random random = new Random();

        public GA(int populationSize, int chromosomeSize, double crossoverRate, double mutationRate) {
            this.populationSize = populationSize;
            this.chromosomeSize = chromosomeSize;
            this.crossoverRate = crossoverRate;
            this.mutationRate = mutationRate;
        }

        public Individual[] initPop() {
            Individual[] population = new Individual[populationSize];
            for (int i = 0; i < populationSize; i++) {
                population[i] = new Individual(chromosomeSize);
            }
            return population;
        }

        public void evaluatePop(Individual[] population) {
            for (int i = 0; i < population.length; i++) {
                population[i].evaluate();
            }
        }

        public Individual[] tournament(Individual[] population) {
            Individual[] selectionTemp = new Individual[populationSize];
            for (int i = 0; i < population.length; i++) {
                Individual parent1 = population[random.nextInt(population.length)];
                Individual parent2 = population[random.nextInt(population.length)];
                if (parent1.getFitness() >= parent2.getFitness()) {
                    selectionTemp[i] = parent1;
                } else {
                    selectionTemp[i] = parent2;
                }
            }
            population = selectionTemp;
            return population;
        }

        public Individual[] crossover(Individual[] population) {
            for (int i = 0; i < population.length - 1; i += 2) {
                Individual offspring1 = new Individual(population[0].getChromosome().length);
                Individual offspring2 = new Individual(population[0].getChromosome().length);
                int xpoint = 1 + random.nextInt(chromosomeSize - 1);
                if (random.nextDouble() < crossoverRate) {
                    for (int j = 0; j < xpoint; j++) {
                        offspring1.setGene(j, population[i].getGene(j));
                        offspring2.setGene(j, population[i + 1].getGene(j));
                    }
                    for (int j = xpoint; j < population[0].getChromosome().length; j++) {
                        offspring1.setGene(j, population[i + 1].getGene(j));
                        offspring2.setGene(j, population[i].getGene(j));

                    }
                }
                population[i] = offspring1;
                population[i + 1] = offspring2;
            }
            return population;
        }

        public Individual[] mutate(Individual[] population) {
            for (int i = 0; i < population.length; i++) {
                for (int j = 0; j < population[i].getChromosome().length; j++) {
                    if (random.nextDouble() < mutationRate) {
                        population[i].mutate(j);
                    }
                }
            }
            return population;
        }

        public Individual[] elitism(Individual[] population) {
            Individual min = population[0];
            int minOffset = 0;
            for (int i = 0; i < population.length; i++) {
                if (population[i].getFitness() <= min.getFitness()) {
                    min = population[i];
                    minOffset = i;
                }
            }
            Individual max = population[0];
            int maxOffset = 0;
            for (int i = 0; i < population.length; i++) {
                if (population[i].getFitness() >= max.getFitness()) {
                    max = population[i];
                    maxOffset = i;
                }
            }
            population[minOffset] = population[maxOffset];
            return population;
        }

        // <editor-fold defaultstate="collapsed" desc="Debug logic...">
        public int totalFitness(Individual[] population) {
            int population_fitness = 0;
            for (int i = 0; i < population.length; i++) {
                population_fitness += population[i].getFitness();
            }

            return population_fitness;
        }

        public double avgFitness(Individual[] population) {
            return (double) totalFitness(population) / population.length;
        }

        public int bestFitness(Individual[] population) {
            int max = population[0].getFitness();
            for (int i = 0; i < population.length; i++) {
                if (population[i].getFitness() > max) {
                    max = population[i].getFitness();
                }
            }
            return max;
        }

        public Individual bestIndividual(Individual[] population) {
            Individual max = population[0];
            for (int i = 0; i < population.length; i++) {
                if (population[i].getFitness() >= max.getFitness()) {
                    max = population[i];
                }
            }
            return max;
        }

        public void printFitness(Individual[] population) {
            System.out.println("Total fitness: " + totalFitness(population));
            System.out.println("Average fitness: " + avgFitness(population));
            // System.out.println("Best fitness: " + bestFitness(population));
            System.out.println("Best individual: " + bestIndividual(population));
        }

        public void printPop(Individual[] population) {
            for (int i = 0; i < population.length; i++) {
                System.out.println(Arrays.toString(population));
            }
        }

        private class Individual {
            public int[] chromosome;
            public int fitness = 0;

            Random random = new Random();

            public Individual(int chromosomeSize) {
                this.chromosome = new int[chromosomeSize];
                for (int i = 0; i < chromosomeSize; i++) {
                    this.setGene(i, random.nextInt(2));
                }
            }

            // Initializes individual with a blank chromosome (all genes 0)
            public Individual(int chromosomeSize, boolean isBlank) {
                this.chromosome = new int[chromosomeSize];
                Arrays.fill(chromosome, 0);
            }

            public void mutate(int offset) {
                if (this.getGene(offset) == 1) {
                    this.setGene(offset, 0);
                } else {
                    this.setGene(offset, 1);
                }
            }

            public void evaluate() {
                int count = 0;
                for (int offset = 0; offset < this.chromosome.length; offset++) {
                    if (this.getGene(offset) == 1) {
                        count++;
                    }
                }
                this.setFitness(count);
            }

            public int getGene(int offset) {
                return this.chromosome[offset];
            }

            public void setGene(int offset, int gene) {
                this.chromosome[offset] = gene;
            }

            public int[] getChromosome() {
                return chromosome;
            }

            public int getFitness() {

                return fitness;
            }

            public void setFitness(int fitness) {
                this.fitness = fitness;
            }

    @Override
    public String toString() {
    String output = "Binary gene representation: ";
    for (int i = 0; i < this.chromosome.length; i++) {
    output += this.getGene(i);
    }
    System.out.println(output);
    System.out.println("Fitness: " + this.getFitness());
    return output;
}