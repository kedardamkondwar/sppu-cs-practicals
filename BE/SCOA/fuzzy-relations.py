class FuzzyRelation:
	def __init__(self, data):
		self.data = data	
		self.shape = (len(data), len(data[0]))
		
	def max_min_composition(self, other):
		data = [[0 for _ in range(other.shape[1])] for _ in range(self.shape[0])]
		for i in range(self.shape[0]):
			for k in range(other.shape[1]):
				data[i][k] = max([min(self.data[i][j], other.data[j][k]) for j in range(self.shape[1])])
		return FuzzyRelation(data)

	def max_product_composition(self, other):
		data = [[0 for _ in range(other.shape[1])] for _ in range(self.shape[0])]
		for i in range(self.shape[0]):
			for k in range(other.shape[1]):
				data[i][k] = max([round(self.data[i][j] * other.data[j][k], 2) for j in range(self.shape[1])])
		return FuzzyRelation(data)

	def __str__(self):
		return '\n'.join(str(row) for row in self.data)

if __name__ == '__main__':
	R1 = FuzzyRelation([list(map(float, line.split())) for line in open('relation1.txt').readlines()])

	R2 = FuzzyRelation([list(map(float, line.split())) for line in open('relation2.txt').readlines()])

	Rmm = R1.max_min_composition(R2)
	print('Max Min Composition:')
	print(Rmm)

	print()

	Rmp = R1.max_product_composition(R2)
	print('Max Product Composition')
	print(Rmp)
