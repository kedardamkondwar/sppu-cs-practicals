import numpy as np
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt

# Creating the points in numpy
X = np.array([
    [10],
    [9],
    [2],
    [15],
    [10],
    [16],
    [11],
    [16]
])

y = np.array([
    [95],
    [80],
    [10],
    [50],
    [45],
    [98],
    [38],
    [93]
])

lr = LinearRegression()
lr.fit(X, y)

plt.scatter(X, y, color='black')
plt.plot(X, lr.predict(X), color='blue', linewidth=3)
plt.show()

print('Line equation is ' + str(round(lr.coef_[0][0], 2)) + '*x + (' + str(round(lr.intercept_[0], 2)) + ')')