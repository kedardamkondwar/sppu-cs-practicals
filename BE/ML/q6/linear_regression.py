import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.datasets import load_boston

X = load_boston()['data']

y = load_boston()['target']

lr = LinearRegression()
lr.fit(X, y)

print('Accuracy of model is ' + str(round(lr.score(X, y)*100, 2)) + '%')

equation = []
for i, coef in enumerate(lr.coef_):
    equation.append('(' + str(round(coef, 2)) + ')*x' + str(i+1))

print('Line eqaution is ' + ' + '.join(equation) + ' + (' + str(round(lr.intercept_, 2)) + ')')