import numpy as np
from sklearn.linear_model import LinearRegression, Ridge, Lasso
from sklearn.model_selection import cross_val_score
from sklearn.datasets import load_boston

X = load_boston()['data']

y = load_boston()['target']

lr = LinearRegression()
scores = cross_val_score(lr, X, y, cv=5)
print('Score for Linear Regression is ' + str(round(scores.mean()*100, 2)))

lrr = Ridge()
scores = cross_val_score(lrr, X, y, cv=5)
print('Score for Ridge Regression is ' + str(round(scores.mean()*100, 2)))

lrl = Lasso()
scores = cross_val_score(lrl, X, y, cv=5)
print('Score for Lasso Regression is ' + str(round(scores.mean()*100, 2)))