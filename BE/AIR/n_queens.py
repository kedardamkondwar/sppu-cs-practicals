class NQueens:
	def __init__(self, n):
		self.n = n
		self.board = [[False for _ in range(n)] for _ in range(n)]
		self.queens = []
		
	def is_left_diagonal_safe(self, r, c):			
		while r >= 0 and c >= 0:
			if self.board[r][c]:
				return False
			r -= 1
			c -= 1
		return True
	
	def is_right_diagonal_safe(self, r, c):			
		while r >= 0 and c < self.n:
			if self.board[r][c]:
				return False
			r -= 1
			c += 1
		return True
		
	def is_column_safe(self, c):
		return not True in [x for x in [self.board[i][c] for i in range(self.n)]]
	
	def is_row_safe(self, r):
		return not True in self.board[r]
		
	def check_queen(self, r, c):
		return self.is_left_diagonal_safe(r, c) and self.is_right_diagonal_safe(r, c) and self.is_column_safe(c) and self.is_row_safe(r)
	
	def check_board(self, r, c):
		if r >= n:
			return
		safe = False
		while (c < self.n):
			safe = self.check_queen(r, c)
			if safe:
				self.board[r][c] = True
				self.queens.append((r, c))
				break
			c += 1
		if safe:
			self.check_board(r+1, 0)
		else:
			qr, qc = self.queens.pop()
			self.board[qr][qc] = False
			self.check_board(qr, qc+1)
			
	def print_board(self):
		self.check_board(0, 0)
		for row in self.board:
			for col in row:
				if col:
					print('Q ', end='')
				else:
					print('_ ', end='')
			print()
	
	
if __name__ == '__main__':
	n = int(input('Enter number of queens: '))
	NQueens(n).print_board()
