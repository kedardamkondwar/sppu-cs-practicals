from sys import maxsize, minsize

class Node:
	def __init__(self, name, value=0, children=[]):
		self.name = name
		self.value = value
		self.children = children
		
	def add_child(self, child):
		self.children.append(child)

def alpha_beta_pruning(node, depth, alpha, beta, max_player):
	if depth == 0 or node.children == []:
		return node.value
	if max_player:
		value = minsize
		for child in node.children:
			value = max(value, alphabeta(child, depth-1, alpha, beta, False))
			alpha = max(alpha, value)
			if alpha >= beta:
				break
		return value
	else:
		value = maxsize
		for child in node.children:
			value = min(value, alphabeta(child, depth-1, alpha, beta, True))
			beta = min(beta, value)
			if alpha >= beta:
				break
		return value
		
if __name__ == '__main__':
	nodes = {}
	
	for name in 'ABCDEFGHIJ':
		nodes[name] = Node(name)
	
	for name, value in zip(
		'KLMNOPQRSTU',
		[2, 3, 5, 9, 0, 7, 4, 2, 1, 5, 6]
	):
		nodes[name] = Node(name, value)
		
	nodes['E'].add_child(nodes['
		
