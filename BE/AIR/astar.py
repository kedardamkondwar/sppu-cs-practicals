from math import INT_MAX as maxv
class Node:
	def __init__(self, name, gval=maxv, hval=0):
		self.name = name
		self.gval = gval
		self.hval = hval
		self.neighbours = {}
	
	def set_neighbours(self, neighbours):
		for neighbour, distance in neighbours.items():
			self.neighbours[neighbour] = distance
			
	def set_gval(self, gval):
		self.gval = gval

	def __repr__(self):
		return self.name
		
	def __str__(self):
		return self.name

def best_first_search(nodes, start='S', goal='G'):
	start = nodes[start]
	start.set_gval(0)
	goal = nodes[goal]
	
	open_list = [start]
	closed_list = []
	
	while goal not in closed_list:
		min_nodes = sorted(open_list, key=lambda x:x.gval + x.hval)
		i = 0
		curr_node = min_nodes[i]
		while curr_node in closed_list:
			i += 1
			curr_node = min_nodes[i]
		closed_list.append(curr_node)
		open_list.extend([x for x in curr_node.neighbours if x not in closed_list])
		open_list.remove(curr_node)
		
	print('->'.join([n.name for n in closed_list]))

if __name__ == '__main__':
	graph = {}

	print('Enter number of nodes: ', end='')
	nodes = int(input())
	
	node_names = ['S', *list('ABCDEFHIJKLMNOPQRTUVWXYZ')[:nodes-2], 'G']
	
	print('Enter Adjacency Matrix:')
	matrix = [list(map(int, input().strip().split() for _ in range(nodes)]
	
	for name_index, name in node_names:
		print(f'Enter heuristic value for {name}: ', end='')
		hval = int(input())
		graph[name] = Node(name, hval=hval)
		neighbours = {}
		for index, distance in enumerate(matrix[name_index]):
			if distance != -1:
				neighbours[node_names[index]] = distance
		graph[name].set_neighbours(neighbours)
		
		
	
