class Graph:
	def __init__(self, vertices):
		self.vertices = vertices
		self.matrix = [[0 for _ in range(vertices+1)] for _ in range(vertices+1)]
		self.all_paths = []
		
	def add_edge(self, v, w, d):
		self.matrix[v][w] = d
		#self.matrix[w][v] = d
		
	def BFS(self, s, g):
		visited = [False for _ in range(self.vertices+1)]
		queue = []
		
		visited[s] = True
		queue.append([s, 0, [s]])
		
		while queue:
			curr_vertex, curr_distance, curr_path = queue[0]
			queue = queue[1:]
			
			for vertex, distance in enumerate(self.matrix[curr_vertex]):
				if distance:
					if vertex == g:
						self.all_paths.append([curr_distance+distance, curr_path+[vertex]])
						#break
					if not visited[vertex]:
						visited[vertex] = True
						queue.append([vertex, distance + curr_distance, curr_path+[vertex]])
				
		for path in sorted(self.all_paths, key=lambda x:x[0]):
			print(path)
				
	def incorrect_dijkstra(self, s, g):
		#visited = [False for _ in range(self.vertices+1)]
		queue = []
		
		#visited[s] = True
		queue.append([s, 0, [s]])
		
		while queue:
			curr_vertex, curr_distance, curr_path = queue[0]
			queue = queue[1:]
			
			for vertex, distance in enumerate(self.matrix[curr_vertex]):
				if distance:
					if vertex == g:
						self.all_paths.append([curr_distance+distance, curr_path+[vertex]])
					queue.append([vertex, distance + curr_distance, curr_path+[vertex]])
				
		for path in sorted(self.all_paths, key=lambda x:x[0]):
			print(path)
			
	def a_star(self, s, g):
		pass
		'''
		visited = [False for _ in range(self.vertices)]
		priority_queue = []
		
		visited[s] = True
		priority_queue.append([s, 0, [s]])
		
		while queue:
			curr_vertex, curr_distance, curr_path = queue[0]
			priority_queue = priority_queue[1:]
			
			for vertex, distance in enumerate(self.matrix[curr_vertex]):
				if distance:
					if vertex == g:
						self.all_paths.append([curr_distance+distance, curr_path+[vertex]])
						break
					if not visited[vertex]:
						visited[vertex] = True
						priority_queue.append([vertex, distance + curr_distance, curr_path+[vertex]])
			priority_queue.sort(key=lambda x:x[1])
				
		for path in self.all_paths:
			print(path)
		'''

if __name__ == '__main__':
	graph = Graph(6)
	s = 0
	a = 1
	b = 2
	c = 3
	d = 4
	g = 5
	graph.add_edge(s,a,1)
	graph.add_edge(a,b,3)
	graph.add_edge(a,c,1)
	graph.add_edge(b,d,3)
	graph.add_edge(c,d,1)
	graph.add_edge(c,g,2)
	graph.add_edge(d,g,3)
	graph.add_edge(s,g,12)
	
	print('\nBFS:')
	graph.BFS(1,5)
	




