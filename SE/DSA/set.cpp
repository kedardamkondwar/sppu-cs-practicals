#include<iostream>
#define SIZE 10 // Size of class set internally
using namespace std;

class Set{
private:
	// Array of students where 1 denotes that the index+1 roll number
	// plays that sport. sport string denotes the sport that this set
	// represents.
	int students[SIZE];
	string sport;

public:
	// This constructor is the default one and asks for the roll numbers of
	// the students to be entered in the object property.
	Set(string sport){
		// Set this object's sport property as the sport obtained from
		// the argument.
		this->sport = sport;
		// Initialise the students BITMASK to set that no students play
		// this object's sport
		for(int i = 0; i < SIZE; i++){
			students[i] = 0;
		}

		cout << "Please enter roll numbers of students for " << sport;
		cout << ": (Enter 0 to stop) " << endl;

		// Since we do not explicitly know how large the input will be we use
		// a variable take one input as there will be atleast one input and 
		// use that variable in a while loop.
		int student;
		cin >> student;
		while(student != 0){
			// Maximum roll numbers are pre set and input exceeding them are
			// invalid.
			if(student > SIZE){
				cout << "Please enter Roll number less than " << SIZE << endl;
			}
			// Set the object's students' array value as 1 if the student plays
			// the sport.
			this->students[student-1] = 1;
			cin >> student;
		}
		// printing to ensure that the input is correct. May be removed.
		this->print();
	}

	// Since union and intersection return Sets, this constructor is
	// created to be used when the students' array values are not from
	// input
	Set(string sport, int* students){
		this->sport = sport;
		for(int i = 0; i < SIZE; i++){
			this->students[i] = students[i];
		}
	}

	// Union is used to find elements that can be in EITHER of the two sets
	Set setUnion(Set other){
		// We want to return a set and thus we require an aray that can be
		// used to get the required BITMASK
		int setStudents[SIZE];
		for(int i = 0; i < SIZE; i++){
			// We use OR here to ensure that even if one sport is played
			// the student will be included in this set
			if(this->students[i] || other.students[i]){
				setStudents[i] = 1;
			} else {
				setStudents[i] = 0;
			}
		}

		// Set is created here using the appropriate constructor and then
		// returned to the caller
		Set either("cricket or badminton", setStudents);

		return either;
	}

	Set setIntersection(Set other){
		int setStudents[SIZE];
		for(int i = 0; i < SIZE; i++){
			if(this->students[i] && other.students[i]){
				setStudents[i] = 1;
			} else {
				setStudents[i] = 0;
			}
		}

		Set both("cricket and badminton", setStudents);

		return both;
	}

	void print(void){
		cout << "Sport: " << this->sport << endl;
		cout << "Students: ";
		for(int roll = 0; roll < SIZE; roll++){
			if(this->students[roll]){
				cout << roll+1 << " ";
			}
		}
		cout << endl;
	}
};

int main(){
	cout << "Sets using C++" << endl;
	cout << "Sizes set internally!!!" << endl;

	Set cricket("cricket");
	Set badminton("badminton");

	cricket.print();

	badminton.print();

	Set both = cricket.setIntersection(badminton);
	both.print();

	Set either = cricket.setUnion(badminton);
	either.print();
}