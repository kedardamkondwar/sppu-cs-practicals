/*In Second year Computer Engineering class of M students, set A of students play cricket and set B of students play badminton. Write C/C++ program to find and display-
i. Set of students who play either cricket or badminton or both
ii. Set of students who play both cricket and badminton
iii. Set of students who play only cricket
iv. Set of students who play only badminton
v. Number of students who play neither cricket nor badminton
(Note- While realizing the set duplicate entries are to avoided)
*/
#include<iostream>
using namespace std;
class student
{
	int c[10],b[10];
public:
	static int countC;
	static int countB;
	int getdata();
	int uni();
	int inter();
	int complement();
	int badminton();
	int cricket();
	student()
	{
		for(int i=0;i<10;i++)
		{
			c[i]=b[i]=0;

		}
	}
};
int student::countB=0;
int student::countC=0;

int student::getdata()
{
	int rollno;int i;

	while(i)
	{
		cout<<"\nEnter the roll no of students who play cricket...Enter -1 to stop";
		cin>>rollno;
		if(rollno==-1)
			break;
		c[rollno]=1;
		student::countC++;
	}

	while(i)
	{
		cout<<"\nEnter the roll no of students who play badminton...Enter -1 to stop";
		cin>>rollno;
		if(rollno==-1)
			break;
		b[rollno]=1;
		student::countB++;


	}


 }
int student::uni()
{
	int cnt=0;
	for(int i=0;i<10;i++)
	{
		if(b[i]==1 || c[i]==1)
			cnt++;
	}return cnt;
}
int student::inter()
{
	int cnt=0;
	for(int i=0;i<10;i++)
	{
		if(b[i]==1 && c[i]==1)
			cnt++;
	}return cnt;
}
int student:: complement()
{
	int cnt=0;
	for(int i=0;i<10;i++)
	{
		if(b[i]==0 && c[i]==0)
			cnt++;
	}
	return cnt;
}
int student:: badminton()
{
	int cnt=0;
	for(int i=0;i<10;i++)
	{
		if(b[i]==1 && c[i]==0)
			cnt++;
	}
	return cnt;
}
int student:: cricket()
{
	int cnt=0;
	for(int i=0;i<10;i++)
	{
		if(b[i]==0 && c[i]==1)
			cnt++;
	}
	return cnt;
}
int main()
{
	int n,m;
	student a3;
	a3.getdata();
	n=a3.uni();
	cout<<"\nThe Union count is:"<<n;
	m=a3.inter();
	cout<<"\nThe Intersection count is:"<<m;
	int d=10-n;
	cout<<"\nComplement"<<d;
	cout<<"\nCricket"<<student::countC;
	cout<<"\nBadminton"<<student::countB;
	return 0;

}
